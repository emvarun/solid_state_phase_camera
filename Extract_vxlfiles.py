import numpy as np
import sys
import os
import Voxel

###################################################################
def extractPhaseFromVXL(filename):
    global camsys, rows, cols
    r = Voxel.FrameStreamReader(filename, camsys)
    if not r.isStreamGood():
        print("Stream is not good: " + filename)
    numFrames = r.size()
    phases = np.zeros((numFrames, rows, cols), dtype='uint16')
    amplitudes = np.zeros((numFrames, rows, cols), dtype='uint16')
    flags = np.zeros((numFrames, rows, cols), dtype='uint16')
    ambient = np.zeros((numFrames, rows, cols), dtype='uint16')
    for i in range(numFrames):
        if not r.readNext():
            print("Failed to read frame %d" %i)
            break
        tofFrame = Voxel.ToF1608Frame.typeCast(r.frames[Voxel.DepthCamera.FRAME_RAW_FRAME_PROCESSED])
        phases[i] = np.array(tofFrame._phase, copy=True).reshape((rows, cols))
        amplitudes[i] = np.array(tofFrame._amplitude, copy=True).reshape((rows, cols))
        ambient[i] = np.array(tofFrame._ambient, copy=True).reshape((rows, cols))
        flags[i] = np.array(tofFrame._flags, copy=True).reshape((rows, cols))
    r.close()
    return phases, amplitudes, ambient, flags

###################################################################
def readinfile(inFileName):
    global camsys, rows, cols
    outFileNamePhases = os.path.splitext(inFileName)[0] + '_phases.npy'
    outFileNameAmplitudes = os.path.splitext(inFileName)[0] + '_amplitudes.npy'
    outFileNameAmbients = os.path.splitext(inFileName)[0] + '_ambients.npy'
    outFileNameFlags = os.path.splitext(inFileName)[0] + '_flags.npy'
    rows = 240
    cols = 320
    camsys = Voxel.CameraSystem()

    phases, amplitudes, ambients, flagsAll = extractPhaseFromVXL(inFileName)
    np.save(outFileNamePhases, phases)
    np.save(outFileNameAmplitudes, amplitudes)
    np.save(outFileNameAmbients, ambients)
    np.save(outFileNameFlags, flagsAll)
    return outFileNamePhases, outFileNameAmplitudes, outFileNameAmbients

###################################################################
def generate_csv_per_frame(param, fileName, dataDir):
    '''
    param=['Ambient','Amplitude','AmplitudeAvg','AmplitudeStd','Depth','DepthAvg','DepthStd','Distance','Phase','PhaseAvg','PhaseStd','PointCloud']
    '''

    deviceResolution=dict()
    device='OPT8241'
    deviceResolution[device]=(240,320)
    fileSizeQ=np.product(deviceResolution[device])

    fileDetails=dict()
    fileDetails[param]=dict()
	
    if param == 'Ambient':
        fileDetails[param]['dtype']=np.uint16
    else:
        fileDetails[param]['dtype']=np.uint16

    fileDetails[param]['nQuantities']=1				        # Only 1 Quantity per pixel stored
    fileDetails[param]['quantityNames']=np.array(['raw'])               # This is just for file naming
    fileDetails[param]['reverseRowCols']=True			        # This is to indicate whether the row and col order is reversed or not

    
    dtype=fileDetails[param]['dtype']					
    nQuantities=fileDetails[param]['nQuantities']		
    quantityName=fileDetails[param]['quantityNames']

    filePath=os.path.join(dataDir,fileName)
    if(os.path.exists(filePath)):				        # Checking if file path exisits
        data=np.fromfile(filePath,dtype=dtype)		                # Loading Raw binary file from disk
        if(np.size(data)%(fileSizeQ*nQuantities)==0):	                # Checking if file size is quantied to whole frames
            nFrames=np.size(data)/(fileSizeQ*nQuantities)               # Calculating number of frames in .bin file  
            print "INFO: %04d Frames in file [%s]"%(nFrames,filePath)
        else:
            nFrames=np.int(np.size(data)/(fileSizeQ*nQuantities))
            temp = len(data)-fileSizeQ*nFrames
            data=data[temp:]
            if(np.size(data)%(fileSizeQ*nQuantities)==0):
                print "INFO: %04d Frames in file [%s]"%(nFrames,filePath)
            else:
                print "ERROR: File size [%d] for file [%s] not a multiple of frame size [%d] X nQuantities [%d]"%(np.size(data),filePath,fileSizeQ,nQuantities)
                return
    else:
        print "ERROR: Unable to open file [%s]"%(filePath)
    
    csvDir=os.path.join(dataDir,fileName.replace('.','_'))		        # Directory where CSV files will be saved
    if(not os.path.exists(csvDir)):					        # Checking for existance of directory if not will be created
        os.makedirs(csvDir)							# Creation of directory
        print 'CREATING DIRECTORY...\n\t', csvDir
    if(fileDetails[param]['reverseRowCols']):
        data=data.reshape(nFrames,deviceResolution[device][0],deviceResolution[device][1],nQuantities).swapaxes(-1,-2).swapaxes(-2,-3)
    else:
        data=data.reshape(nFrames,deviceResolution[device][1],deviceResolution[device][0],nQuantities).swapaxes(-1,-3)
		
    for c0 in np.arange(nFrames):						# Loop for each frame
        for c1 in np.arange(nQuantities):				        # Loop for each quantity per pixel 
            dataSave=data[c0,c1]					        # Extracted data selected only for 1 frame and 1 quantity
            csvFileName=csvDir+'/frame%04d_%s.csv'%(c0,quantityName[c1])
        np.savetxt(csvFileName,dataSave,delimiter=',',newline='\n')             # Saving in CSV format

###################################################################
def read_vxl_save_csv(rootdir, vxl_filename, read_vxl=False):
    if read_vxl:
        file2analyse=os.path.join(rootdir, vxl_filename)
        Phases, Amplitudes, Ambients=readinfile(file2analyse)
    else:
        file2analyse=os.path.join(rootdir, vxl_filename)
        Phases = os.path.splitext(file2analyse)[0] + '_phases.npy'
        Amplitudes = os.path.splitext(file2analyse)[0] + '_amplitudes.npy'
        Ambients = os.path.splitext(file2analyse)[0] + '_ambients.npy'

    generate_csv_per_frame('Ambient', Ambients, rootdir)
    generate_csv_per_frame('Amplitude', Amplitudes, rootdir)
    generate_csv_per_frame('Phase', Phases, rootdir)

###################################################################
rootdir='E:\Low_Noise_ModDepth'
#vxl_filename='bkgtest_3v3benchtop_disconmixerout.vxl'
read_vxl=True

for dirpath, dirname, files in os.walk(rootdir):
    for afile in files:
        if afile.find('.vxl')>0:
            print dirpath, '\t\t\t', afile
            read_vxl_save_csv(dirpath, afile, read_vxl)
