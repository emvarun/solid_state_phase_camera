#!/usr/bin/env python2.7
#
# TI Voxel Lib component.
# 
# Copyright (c) 2014 Texas Instruments Inc.
#

import Voxel
import cv2
import matplotlib.pyplot as plt
import numpy as np

#####################################################
sys = Voxel.CameraSystem()
devices = sys.scan()
#####################################################
count = 1
print "CAPTURING FRAME NO. \t", count

gdepthCamera = None
pcframe = None
gframe = None

global cap_type
cap_type = 'DEPTH'
#cap_type = 'IQ'
loop = True
#####################################################
def get_color_scale(param):
  lb=np.min(param)
  ub=np.max(param)
  return lb, ub
#####################################################
def callback(depthCamera, frame, type):
  global count 
  global gdepthCamera, pcframe, gframe
  print frame.id, "\t@\t", frame.timestamp, depthCamera.getFrameRate(),"\t attempting typeCast"
  rawFrame = Voxel.DepthFrame.typeCast(frame)
  
  ## Converting to Phase and Amplitude
  phase = np.array(rawFrame.depth)
  print phase
  phase = phase.reshape(240,320)
  amplitude = np.array(rawFrame.amplitude)
  amplitude = amplitude.reshape(240,320)
  
  phase = phase*100.;
  phase = phase.astype('uint16');
  amplitude = amplitude*1000;
  amplitude = amplitude.astype('uint16');
  
  ## Refresh the window with the new phase and confidence maps
  cmap = plt.get_cmap('jet')
  amplitude = cmap(amplitude)
  cv2.imshow('Confidence',amplitude), cv2.waitKey(1)

  cmap = plt.get_cmap('hot')
  phase = cmap(phase)
  cv2.imshow('Phase',phase), cv2.waitKey(1)
  count = count + 1
  #print "CAPTURING FRAME NO. \t", count
  
  nFrames=50
  if count > nFrames:
    gdepthCamera.stop()
    #del gdepthCamera
    pcframe = Voxel.RawFrame.typeCast(frame)
    gframe = frame
    return gframe
#####################################################
def init_frame_params(devices):
  gCamera=sys.connect(devices[0])
  frameSize = Voxel.FrameSize()
  frameSize.height = 240
  frameSize.width = 320
  gCamera.setFrameSize(frameSize)
  return gCamera, frameSize 
#####################################################
def Update_Registers(camera, prog):
  global cap_type
  # Set the registers to read out the raw quad data
  print prog.readRegister(0x5802), prog.readRegister(0x5838), prog.readRegister(0x5826)  
  camera.setCameraProfile(132)#112
  camera.getb('tg_dis')
  camera.setb('tg_dis',True)
  print 'tg_dis \t', camera.getb('tg_dis')
  camera.geti('sysclk_in_freq')
  camera.seti('sysclk_in_freq', 0)
  camera.geti('op_clk_freq')
  camera.seti('op_clk_freq', 0)
  if True:
    camera.geti('sub_frame_cnt_max')
    camera.seti('sub_frame_cnt_max', 1)
    camera.geti('quad_cnt_max')
    camera.seti('quad_cnt_max', 4)
    camera.getu('pix_cnt_max')
    camera.setu('pix_cnt_max', 480000)#480000)
    camera.getu('intg_time')
    camera.setu('intg_time', 11)

  if False:
    camera.geti('sub_frame_cnt_max')
    camera.seti('sub_frame_cnt_max', 2)
    camera.geti('quad_cnt_max')
    camera.seti('quad_cnt_max', 4)
    camera.getu('pix_cnt_max')
    camera.setu('pix_cnt_max', 240000)#480000)
    camera.getu('intg_time')
    camera.setu('intg_time', 11)
    
  camera.getu('iq_scale')
  camera.setu('iq_scale', 0)  
  #For 3rd March 2020 dataset the value is (1,2) subframe 250000

  #Turning off the feedback loop
  camera.getb('fb_ready_en')
  camera.setb('fb_ready_en', True)
  camera.getb('illum_ovtemp_intr_dis')
  camera.setb('illum_ovtemp_intr_dis', True)
  camera.geti('delay_fb_coeff_1')
  camera.seti('delay_fb_coeff_1', 0)
  camera.geti('delay_fb_dc_corr_mode')
  camera.seti('delay_fb_dc_corr_mode', 0)
  camera.geti('delay_fb_corr_mode')
  camera.seti('delay_fb_corr_mode', 0)

  #Other params
  
  camera.getb('dealias_en');
  camera.setb('dealias_en', False);
  camera.getb('lumped_dead_time');
  camera.setb('lumped_dead_time', True);
  camera.getb('modulation_hold');
  camera.setb('modulation_hold', False);
  camera.geti('op_data_arrange_mode')
  camera.seti('op_data_arrange_mode', 0)
  camera.geti('amplitude_threshold')
  camera.seti('amplitude_threshold', 0)
  camera.geti('output_mode')
  camera.seti('output_mode', 0)
    
  #Re-enabling Timing
  camera.getb('tg_dis')
  camera.setb('tg_dis',False)
  #prog.writeRegister(0x5826, 0x00c000) # WE DON'T NEED THIS! LEAVE COMMENTED!

  #External modulation
  camera.setb('sync_mode', False)
  camera.setb('slave_mode', True)
  prog.writeRegister(0x5838, 0x00cc00) #Setting MSB of this register to a true value enables R/W on modulation input
  prog.writeRegister(0x5802, 0x70)
  
  print prog.readRegister(0x5802), prog.readRegister(0x5838), prog.readRegister(0x5826)
  print 'FEEDBACK REGISTER', prog.readRegister(0x5c3f), prog.readRegister(0x5cb1), prog.readRegister(0x5cb1)
  print camera.getb('fb_ready_pol'), camera.getb('fb_ready_en'),camera.geti('delay_fb_dc_corr_mode'),camera.geti('delay_fb_dc_corr_mode')

  if prog.readRegister(0x5802)[0]:
    print "REGISTERS UPDATED SUCCESSFULLY!!"
  else:
    print "ERROR!! REGISTERS UPDATE FAILED!! \n\t MODULATION TO CAMERA TOO HIGH!!?"
  return camera, prog
#####################################################
if len(devices) > 0:
  gdepthCamera, frameSize = init_frame_params(devices) 
  prog = gdepthCamera.getProgrammer()
  # Clear previous callback functions
  gdepthCamera.clearAllCallbacks()
  # Update Register Setting for gdepthCamera
  gdepthCamera, prog = Update_Registers(gdepthCamera, prog)
  # Register Callback and Initiate Capture
  if cap_type == 'DEPTH':
    gframe = gdepthCamera.registerCallback(Voxel.DepthCamera.FRAME_DEPTH_FRAME, callback)
  if cap_type == 'IQ':
    gframe = gdepthCamera.registerCallback(Voxel.DepthCamera.FRAME_RAW_FRAME_PROCESSED, callback_IQ)

  if loop:
      gdepthCamera.start()
      print "\n\n\t\t STATUS OF CAMERA ACQUIRE:", gdepthCamera.isRunning()
      print "\t\t PRESS CTRL+C to ABORT!!!\n\n"
      
      # Run the gdepthCamera.until the user hits ctrl+c 
      try:
          while gdepthCamera.isRunning():
              if gdepthCamera.isRunning() == False: break
      except KeyboardInterrupt:
          gdepthCamera.stop() # Stop the camera
      
      gdepthCamera.wait()
else:
  print "CAMERA NOT CONNECTED!! \n \t CONNECT CAMERA AND TRY AGAIN.. \n\n"
