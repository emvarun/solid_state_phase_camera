import numpy as np

# Output Frequency
f_out = 25.*10**6
# Clock Frequency
f_clk = 400.*10**6
# Bit_available
f_bit = 2**32

FTW_decimal=f_bit*f_out/f_clk
print "FTW in decimal \t", int(FTW_decimal)

# Output Phases
out_phases = [0, 60, 120, 180, 240, 300]
# Bit_available phase
ph_bit = 2**14
max_ph = 360
# PTW
for aphase in out_phases:
    ptw = ph_bit*aphase/max_ph
    print "PTW in decimal for \t", aphase, 'is \t', int(ptw)

# get max_cnt_pix
frame = 7.105
quad_count=4
sub_frame =1
cam_clk=48*10**6

pix_max = cam_clk/(frame*quad_count*sub_frame)
print "pix_cnt_max \t", pix_max
