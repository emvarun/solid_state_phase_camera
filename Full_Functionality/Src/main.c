/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"
#include "params.h"
  
void SystemClock_Config(void);

 /* Private function prototypes -----------------------------------------------*/
 /* USER DEFINED FUNCTIONS*/
void ResetDDS(void);
void InitDDS(void);
void mod_chan(unsigned char *channel,unsigned char *freq, unsigned char *phase);
void IOupdate(void);
void FF_IOupdate(void);
void Setup_DDS_Quad(void);
void startFrameTrig(void);
void stopFrameTrig(void);
void startcounter(void);
void stopcounter(void);
void Mod_Off_DDS(void);
void four_phase_steps(int quadcount);
void RF_switch_four_phase_steps(int quadcount);
int fix_quadcount(int quadcount, int nQuads);
void DWT_Delay_us(volatile uint32_t microseconds);

HAL_StatusTypeDef HAL_SPI_TransmitNew(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM8_Init();
  MX_TIM3_Init();

	/* Turning Off The Switch */
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET);
	HAL_Delay(100);
  
  /* INITIALIZING THE DDS */	
  InitDDS();
	
	/* Setting the four channels for the switching */
  Setup_DDS_Quad();
	HAL_Delay(1);
  /* Waiting for DDS to stabilize: 10 ms*/
  
  // STARTING UP THE PHASE CAMERA STUFF!!
  startFrameTrig();
	
	/* Testing Switch */
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
	HAL_Delay(100);
	
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
	HAL_Delay(100);

	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
	HAL_Delay(100);

	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
	HAL_Delay(100);
	/* Testing Switch */

  /* Infinite loop */ 
  while (1)
  {

//		FF_IOupdate();
//		HAL_Delay(10);
  }
}

/* DEFINATIONS OF USER DEFINED FUNCTIONS */
/* Reseting the DDS parameters*/
void ResetDDS(void) 
{
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_9,GPIO_PIN_SET); //Need a pulse atleast .266us wide
	HAL_Delay(5);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_9,GPIO_PIN_RESET);
	return;
}
/* Initiallizing the register settings of the DDS */
void InitDDS(void) 
{
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_8,GPIO_PIN_SET);
	HAL_Delay(10);
	ResetDDS();
	HAL_Delay(10);
	IOupdate();
	HAL_SPI_TransmitNew(&hspi1,init0,2,timeout); //Config CSR register
	IOupdate();
	HAL_Delay(10);
	HAL_SPI_TransmitNew(&hspi1,init1,4,timeout); //Config FR1 register
	IOupdate();
	HAL_Delay(10);
	HAL_SPI_TransmitNew(&hspi1,init2,3,timeout); //Config FR1 register
	IOupdate();
	HAL_Delay(10);
	HAL_SPI_TransmitNew(&hspi1,init3,4,timeout); //Config CFR register
	IOupdate();
	return;
}
/* SET DDS PARAMS: 
      channel pointer, frequency register, phase regsiter
*/
/* Modultion Switch Functions */
void mod_chan(unsigned char *channel, unsigned char *freq, unsigned char *phase){
	HAL_SPI_TransmitNew(&hspi1, channel, 2, timeout);
  HAL_SPI_TransmitNew(&hspi1, freq, 5, timeout);
  HAL_SPI_TransmitNew(&hspi1, phase, 3, timeout);	
	IOupdate();
}
void Setup_DDS_Quad(void){
  mod_chan(chan0, f_mod, phase_0);
  mod_chan(chan1, f_mod, phase_90);
  mod_chan(chan2, f_mod, phase_180);
  mod_chan(chan3, f_mod, phase_270);
}
void Mod_Off_DDS(void){
  mod_chan(chan0, f_off, ph_off);
  mod_chan(chan1, f_off, ph_off);
  mod_chan(chan2, f_off, ph_off);
  mod_chan(chan3, f_off, ph_off);
}
/* PB11 */
void IOupdate(void)
{		
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_11,GPIO_PIN_SET);
	for(unsigned char i =0;i<20;i++)
	;;
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_11,GPIO_PIN_RESET);
	return;
}
/* USING Flip-Flop 
		PB12: Clear 
		PB14:	J
*/
void FF_IOupdate(void)
{		
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_14,GPIO_PIN_SET);
	for(unsigned char i =0;i<240;i++)
	;;
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_14,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12,GPIO_PIN_RESET);
	return;
}
/* Fixing the quadcount */
int fix_quadcount(int quadcount, int nQuads)
{
	quadcount++;
	if(quadcount==nQuads)
		quadcount=0;
	return quadcount;
}	
/* Phase Stepping with 90 degree readouts */
void four_phase_steps(int quadcount) 
{
	switch (quadcount){
		case 0: {
			/* Turning on RF1 out: ALL pins set to low 
			*/
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_RESET);
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
			break;
		}
		case 1: {
			/* Turning on RF2 out: Ctrl-1 is HIGH
			*/
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET);
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
			break;
		}
		case 2: {
			/* Turning on RF3 out: Ctrl-3 is HIGH
			*/
			HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_RESET);
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
			break;
		}
		case 3: {
			/* Turning on RF4 out: Ctrl-1 and Ctrl-3 is HIGH
			*/
			HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET);
      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
			break;
		}
	}
}
void DWT_Delay_us(volatile uint32_t microseconds)
{
  uint32_t clk_cycle_start = DWT->CYCCNT;
 
  /* Go to number of cycles for system */
  microseconds *= (HAL_RCC_GetHCLKFreq() / 1000000);
 
  /* Delay till end */
  while ((DWT->CYCCNT - clk_cycle_start) < microseconds);
}
void RF_switch_four_phase_steps(int quadcount) 
{
	switch (quadcount){
		case 0: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);
			HAL_SPI_TransmitNew(&hspi1,phase_0,3,timeout);
			IOupdate();
			break;
		}
		case 1: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_90,3,timeout);
			IOupdate();
			break;
		}
		case 2: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_180,3,timeout);
			IOupdate();
			break;
		}
		case 3: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_270,3,timeout);
			IOupdate();
			break;
		}
	}
	// Wait time for DDS DDS phase-step
	for(unsigned int i =0;i<10000;i++)
	;;
}

void six_phase_steps(int quadcount) 
{
	switch (quadcount){
		case 0: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);
			HAL_SPI_TransmitNew(&hspi1,phase_0,3,timeout);
			IOupdate();
			break;
		}
		case 1: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_60,3,timeout);
			IOupdate();
			break;
		}
		case 2: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_120,3,timeout);
			IOupdate();
			break;
		}
		case 3: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_180,3,timeout);
			IOupdate();
			break;
		}
		case 4: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_240,3,timeout);
			IOupdate();
			break;
		}
		case 5: {
			HAL_SPI_TransmitNew(&hspi1,chan0,2,timeout);
			HAL_SPI_TransmitNew(&hspi1,f_mod, 5,timeout);				
			HAL_SPI_TransmitNew(&hspi1,phase_300,3,timeout);
			IOupdate();
			break;
		}
	}
	// Wait time for DDS DDS phase-step
	for(unsigned int i =0;i<1000;i++)
	;;
}
/* START STOP frame trigger */
void startFrameTrig(void){
	HAL_TIM_Base_Start_IT(&htim8);
	HAL_TIM_OC_Start_IT(&htim8,TIM_CHANNEL_1);
}
void stopFrameTrig(void){
	HAL_TIM_Base_Stop_IT(&htim8);
	HAL_TIM_OC_Stop_IT(&htim8,TIM_CHANNEL_1);
}
void startcounter(void){
	__HAL_TIM_SET_COUNTER(&htim3,0);
	HAL_TIM_Base_Start_IT(&htim3);
}
void stopcounter(void){
	HAL_TIM_Base_Stop_IT(&htim3);
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}
		
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	UNUSED(GPIO_Pin);
	if (GPIO_Pin==GPIO_PIN_6){
		startFrameTrig();
		return;
	}
	if (GPIO_Pin==GPIO_PIN_0){
		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)){
      /* Turn the RF switch on based on a logic
      */
			startcounter();
			if(nQuads==4){
				four_phase_steps(quadcount);
			}
			if(nQuads==6)
				six_phase_steps(quadcount);
			/* if using the tim3 counter */  
			quadcount=fix_quadcount(quadcount, nQuads);
    }
    else{
      /* Turn the RF switch off: Needed only if TIM3 isn't used
			If TIM3 is used: this code is inside the HAL_TIM_PeriodElapsedCallback function
      */
//      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
//      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_SET);
//      HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET); 
			//mod_chan(chan0, f_off, ph_off); //added for FF only
			
    }
	}
}

//TIMER Callbacks
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	// Handle TIM3 update for exposure control
	// USING TIM3 variable "tim3_period" to control the timing of the RF Switch or the duration of modulations.
	if (htim->Instance==TIM3){
		stopcounter();
		/* Turn the RF switch off: Stopping Modulation
    */
    HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE,GPIO_PIN_11,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,GPIO_PIN_SET);
  }
	//Handle TIM8 update for quad offset correction
  if (htim->Instance==TIM8){
		quadcount = 0;
	}
}
/* USER CODE BEGIN 4 */
HAL_StatusTypeDef HAL_SPI_TransmitNew(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout){
	HAL_StatusTypeDef returnvar;
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_8,GPIO_PIN_RESET);
	returnvar = HAL_SPI_Transmit(hspi,pData,Size,Timeout);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_8,GPIO_PIN_SET);
	return returnvar;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
