/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __params_H
#define __params_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

//Timeout for SPI
#define timeout 100
#define TIM_expose 1600
 /* Private variables ---------------------------------------------------------*/
 //Keeps track of current quad inside the frame
static int quadcount = 0;
static int nQuads = 4;

// Format: {Register, [MSB --> LSB]}
/* INITIALIZING DDS CHANNEL REGISTER 0x00 */
// Enables all channels and sets data read order to LSB first
static unsigned char init0[2] = {0x00,0xF0};
/* INITIALIZING DDS CHANNEL REGISTER 0x01 */
static unsigned char init1[4] = {0x01, 0xC0,0x00,0x40};
/* INITIALIZING DDS CHANNEL REGISTER 0x02 */
static unsigned char init2[3] = {0x02, 0x20,0x00};
/* INITIALIZING DDS CHANNEL REGISTER 0x03 */
//Auto Clear Phase Accumulator and sine wave output on last byte
static unsigned char init3[4] = {0x03, 0x00,0x03,0x04};

/* PROGRAMMING DEFAULTS FOR DDS REGISTER 0x00 */
static unsigned char chan[2] = {0x00,0xF0};
static unsigned char chan0[2] = {0x00,0x10};
static unsigned char chan1[2] = {0x00,0x20};
static unsigned char chan2[2] = {0x00,0x40};
static unsigned char chan3[2] = {0x00,0x80};

 /* PROGRAMMING DEFAULTS FOR DDS REGISTER 0x04
    i.e. Frequency Tuning Word FTW
    using 400 MHz DDS system clock.
 */
static unsigned char f_off[5] = {0x04,0x00,0x00,0x00,0x00};
static unsigned char f_25MHz[5] = {0x04,0x10,0x00,0x00,0x00}; //25 MHz
 /* PROGRAMMING DEFAULTS FOR DDS REGISTER 0x05
    i.e. Phase Tuning Word PTW
 */
static unsigned char ph_off[3] = {0x05,0x00,0x00}; //Zero phase setting between ch0 and ch1

 /* DIFFERENT PHASE VALUES USED IN OUR SETUP FOR PHASE STEPPING
    SETTING MODULATION FREQUENCY
 */
static unsigned char f_mod[5] = {0x04,0x10,0x00,0x00,0x00}; //25 MHz
static unsigned char phase_0[3] = {0x05,0x00,0x00};
static unsigned char phase_90[3] = {0x05,0x10,0x00};
static unsigned char phase_180[3] = {0x05,0x20,0x00};
static unsigned char phase_270[3] = {0x05,0x30,0x00};


 static unsigned char phase_60[3] = {0x05,0x0A,0xAA};
 static unsigned char phase_120[3] = {0x05,0x15,0x55};
 static unsigned char phase_240[3] = {0x05,0x2A,0xAA};
 static unsigned char phase_300[3] = {0x05,0x35,0x55};

 /* Timing variables
  */
 /*tim8_period: 
		Controls the Frame-Rate;
		Keeps the Positive Pulse-Width for VD-in constant;
		values below for tim8_prescalar=65535
		120 -> Frame Rate of 21.19 Hz
		185 -> Frame Rate of 13.77 Hz (Default)
		220 -> Frame-Rate of 11.60 Hz
		250 -> Frame-Rate of 10.21 Hz
		360 -> Frame-Rate of 7.102 Hz
		500 -> Frame-Rate of 5.107 Hz
		1000 -> Frame-Rate of 2.559 Hz
 */
 static int tim8_period = 360; 
 
 /*tim8_prescalar: 
		Controls the Frame rate Frequency;
		Changes the positive pulse width accordingly..
		65535 -> Default and highest bit.;
 */
 static int tim8_prescalar = 65535; 
 
 /*tim8_pulse_width:
		values for tim8_prescalar=65535; 
		50  -> 19.50ms (Default)
		150 -> 58.60ms
		200 -> 78.10ms
		250 -> 97.50ms
 */
 static int tim8_pulse_width =  104;//104;

 // tim3_period:
 static int tim3_period = 1000; //0;
 // tim3_prescalar: 
 static int tim3_prescalar = 84; //84;
 
#ifdef __cplusplus
}
#endif
#endif /*__ params_H */
